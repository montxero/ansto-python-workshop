#!/usr/bin/env python


'''missing_digit_fp_test.py'''


import unittest
from missing_digit_fp import (parse_equation,
                              normalise,
                              evaluate_expression,
                              get_missing_digit)


class ParseEquationTest(unittest.TestCase):
    def test_parse_equation(self):
        self.assertEqual(parse_equation('12 - 4 = x'),
                         (('-', '12', '4'), ('x',)))
        self.assertEqual(parse_equation('76 * x4 = 1064'),
                         (('*', '76', 'x4'), ('1064',)))
        self.assertEqual(parse_equation('1x + 30 = 49'),
                         (('+', '1x', '30'), ('49',)))
        self.assertEqual(parse_equation('x / 4 = 2'),
                         (('/', 'x', '4'), ('2',)))


class NormaliseTest(unittest.TestCase):
    def test_missing_digit_on_rhs(self):
        self.assertEqual(normalise((('+', '30', '19'), ('4x',))),
                                   (('+', '30', '19'), ('4x',)))
        self.assertEqual(normalise((('*', '76', '14'), ('1x64',))),
                                    (('*', '76', '14'), ('1x64',)))
        self.assertEqual(normalise((('-', '43', '14'), ('x9',))),
                                    (('-', '43', '14'), ('x9',)))
        self.assertEqual(normalise((('/', '6', '3'), ('x',))),
                                    (('/', '6', '3'), ('x',)))

    def test_missing_digit_in_b(self):
        self.assertEqual(normalise((('+', '30', '1x'), ('49',))),
                                   (('-', '49', '30'), ('1x',)))
        self.assertEqual(normalise((('*', '76', 'x4'), ('1064',))),
                                    (('/', '1064', '76'), ('x4',)))
        self.assertEqual(normalise((('-', '43', '1x'), ('29',))),
                                    (('-', '43', '29'), ('1x',)))
        self.assertEqual(normalise((('/', '6', 'x'), ('3',))),
                                    (('/', '6', '3'), ('x',)))

    def test_missing_digit_in_a(self):
        self.assertEqual(normalise((('+', 'x0', '19'), ('49',))),
                                   (('-', '49', '19'), ('x0',)))
        self.assertEqual(normalise((('*', '7x', '14'), ('1064',))),
                                    (('/', '1064', '14'), ('7x',)))
        self.assertEqual(normalise((('-', 'x3', '14'), ('29',))),
                                    (('+', '29', '14'), ('x3',)))
        self.assertEqual(normalise((('/', 'x', '3'), ('2',))),
                                    (('*', '2', '3'), ('x',)))



class EvaluateExpressionTest(unittest.TestCase):
    def test_evaluate_addition(self):
        expression_result_pairs = {('+', '30', '19') : ('49',),
                                   ('+', '13123', '0') : ('13123',),
                                   ('+', '221876', '1937') : ('223813',),
                                   ('+', '1', '1') : ('2',),
                                   ('+', '0', '0') : ('0',)}
        for k, v in expression_result_pairs.items():
            self.assertEqual(evaluate_expression(k), v)

    def test_evaluate_subtraction(self):
        expression_result_pairs = {('-', '30', '19') : ('11',),
                                   ('-', '13123', '0') : ('13123',),
                                   ('-', '221876', '1937') : ('219939',),
                                   ('-', '1', '1') : ('0',),
                                   ('-', '0', '0') : ('0',)}
        for k, v in expression_result_pairs.items():
            self.assertEqual(evaluate_expression(k), v)

    def test_multiplication(self):
        expression_result_pairs = {('*', '30', '19') : ('570',),
                                   ('*', '13123', '0') : ('0',),
                                   ('*', '4', '55469') : ('221876',),
                                   ('*', '1', '1') : ('1',),
                                   ('*', '0', '0') : ('0',)}
        for k, v in expression_result_pairs.items():
            self.assertEqual(evaluate_expression(k), v)

    def test_division(self):
        expression_result_pairs = {('/', '570', '19') : ('30',),
                                   ('/', '64', '8') : ('8',),
                                   ('/', '221876', '4') : ('55469',),
                                   ('/', '1', '1') : ('1',),
                                   ('/', '0', '100') : ('0',)}
        for k, v in expression_result_pairs.items():
            self.assertEqual(evaluate_expression(k), v)


class GetMissingDigitTest(unittest.TestCase):
    def test_missing_digit_alone_on_rhs(self):
        expression_result_pairs = {'12 - 4 = x' : '8',
                                   '2 + 2 = x'  : '4',
                                   '9 / 3 = x' : '3'}
        for k, v in expression_result_pairs.items():
            self.assertEqual(get_missing_digit(k), v)

    def test_missing_digit_on_rhs(self):
        expression_result_pairs = {'15 * 25 = 3x5' : '7',
                                   '64 / 2 = 3x' : '2',
                                   '1937 + 221876 = 223x13' : '8',
                                   '39 / 3 = x3' : '1',
                                   '23 - 11 = x2' : '1'}
        for k, v in expression_result_pairs.items():
            self.assertEqual(get_missing_digit(k), v)

    def test_missing_digit_on_lhs(self):
        expression_result_pairs = {'1x0 * 12 = 1200' : '0',
                                   '3x / 3 = 13' : '9',
                                   '1937 + 2218x6 = 223813' : '7',
                                   '15 * x5 = 375' : '2',
                                   'x3 - 11 = 12' : '2'}
        for k, v in expression_result_pairs.items():
            self.assertEqual(get_missing_digit(k), v)



if __name__ == '__main__':
    unittest.main()
