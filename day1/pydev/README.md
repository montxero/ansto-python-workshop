# Setup
Navigate to the pydev directory then run the following commands in sequence
```sh
$ poetry virtualenvs.in-project true
$ poetry update
$ poetry shell
```
