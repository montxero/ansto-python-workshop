{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Development Tools\n",
    "At the end of this session, you will understand\n",
    "- Python dependency management with poetry\n",
    "- static type checking\n",
    "- function annotation\n",
    "- testing python programs\n",
    "- linting with pylint\n",
    "\n",
    "\n",
    "This module covers software tools which ease the task of developing Python programs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Project setup and management with Poetry\n",
    "Poetry is a high level tool for managing python projects.\n",
    "With Poetry, you can specify the version of Python, and libraries your project depends on\n",
    "in a simple configuration file called `pyproject.toml`.\n",
    "The configuration file provides an easy way to replicate a project's development environment\n",
    "in several machines.\n",
    "Poetry takes care of setting up a virtual environment for your development environment.\n",
    "\n",
    "Documentation for setting up and using Poetry can be found on the Poetry documentation site\n",
    "https://python-poetry.org/docs/."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Catching Errors During Development\n",
    "## Pylint\n",
    "Pylint is a tool which checks for programming errors and style guide conformance in Python\n",
    "source code.\n",
    "You can specify the stylistic requirements and constraints for a project in a\n",
    "`.pylintrc` configuration file.\n",
    "\n",
    "Pylint can be integrated into several editors and IDEs to check programs on the fly during \n",
    "development.\n",
    "\n",
    "Check out the official documentation for the details of your preferred editor/IDE."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We call pylint from the command line like so:\n",
    "`$ pylint source.py`\n",
    "When called this way, Pylint will analyse the file and give it a score based on the configuration file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def funky_string_reverse(string):\n",
    "    '''Return the reverse of `string`.'''\n",
    "    if string:\n",
    "        return string[::-1]  # Syntactic sugar for reversing sequences"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "        \n",
    " When funky_string_reverse is passed a non-empty string, it returns the reverse of the string.\n",
    "If it is passed an empty string, it returns None."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(type(funky_string_reverse('synchrotron')))\n",
    "print(type(funky_string_reverse('')))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "user_input = input('Please enter your response: ')\n",
    "user_input_reversed = funky_string_reverse(user_input)\n",
    "print('The type of user_input_reversed is', type(user_input_reversed))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Function Annotations\n",
    "In Python, function annotations are means for adding metadata to functions.\n",
    "Function annotations are arbitrary expressions - they do not have to be types.\n",
    "The big idea behind function annotations is to make functions more readable.\n",
    "Annotations do not have any effect on how your program runs.\n",
    "They are completely ignored while your program runs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def foo(x : 'something') -> 'something_else':\n",
    "    return x*x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "foo(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Type Hints\n",
    "A common use for function annotations is to document the expected type of a function.\n",
    "The best way to get to grips with type hints is to see one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def exclaim(in_str: str) -> str:\n",
    "    '''return in_str with an exclamation mark at the end'''\n",
    "    return ''.join((in_str, '!'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exclaim('hey')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `exclaim` is annotated to indicate that it takes a `str` and returns a `str`.\n",
    "It is important to note that the annotation is not enforced at all.\n",
    "\n",
    "To drive home this point, consider the following example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add(x: int, y: int) -> float:\n",
    "    return x + y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The annotation of `add` indicates that it takes parameters of type `int` and returns a float.\n",
    "Let us do something jarring."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "add('Annotations are', ' IGNORED!!!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we see, the type hints are completely ignored.\n",
    "No error was raised, no warning was given.\n",
    "It begs the question: what is the point?\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Types and Type Systems\n",
    "Type systems provide a means of determining the type of **every** value, expression and \n",
    "computation in a program **by only syntactic analysis** of the program.\n",
    "\n",
    "In simple terms, with a type system, we can analyse the source code of a program and tell\n",
    "the type of every value and expression without running the program.\n",
    "\n",
    "We saw earlier that in a dynamically typed language, we cannot infer the type of every\n",
    "value by static analysis.\n",
    "\n",
    "Python does not have a type system.\n",
    "However, there are tools to perform static type checking on Python programs.\n",
    "The de facto standard of these is Mypy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Static type checking with Mypy\n",
    "Mypy provides an optional type system for Python.\n",
    "Mypy checks that functions with type hints are used properly.\n",
    "With Mypy, you can annotate only the parts of your code base you want to perform static\n",
    "type checks for.\n",
    "\n",
    "This approach of introducing static type checking to dynamicaly typed languages is known as\n",
    "**gradual typing**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The syntax for running mypy on a source file is\n",
    "```sh\n",
    "$ mypy source.py\n",
    "```\n",
    "\n",
    "To run mypy on a directory of Python files,\n",
    "```sh\n",
    "$ mypy directory-containing-at-least-one-python-file\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider a program with the snippet:\n",
    "\n",
    "```python\n",
    "def add(x: int, y: int) -> int:\n",
    "    return x + y\n",
    "\n",
    "add('Annotations are', ' IGNORED!!!')\n",
    "add(2, 3)\n",
    "```\n",
    "When Mypy is run on the program, it reports the following errors:\n",
    "```\n",
    "error: Argument 1 to \"add\" has incompatible type \"str\"; expected \"int\"\n",
    "error: Argument 2 to \"add\" has incompatible type \"str\"; expected \"int\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Testing\n",
    "Writing and running tests is a very important way to verify that your program works as \n",
    "intended.\n",
    "Python provides several faccilities for writing and running tests."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Doctests\n",
    "The `doctest` module included in the Python stand library provides a simple way to \n",
    "run tests incorporated docstrings.\n",
    "\n",
    "The syntax for doctests is as follows\n",
    "```python\n",
    "'''Doctests appear in multiline strings.\n",
    ">>> expression 1\n",
    "expected result\n",
    ">>> expression 2\n",
    "expected result\n",
    "'''\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the following module (stand alone program file) which\n",
    "you can find at `pydev/src/doctest_example/doctest_example.py`:\n",
    "```python\n",
    "'''doctest_example.py a simple illustration of doctests.'''\n",
    "\n",
    "\n",
    "def integral_part(x):\n",
    "    '''Return the integral part of x.\n",
    "\n",
    "    Parameter\n",
    "    ---------\n",
    "    x :  number\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    float\n",
    "\n",
    "    Examples\n",
    "    --------\n",
    "    >>> integral_part(4)\n",
    "    4\n",
    "    >>> integral_part(3.56)\n",
    "    3\n",
    "    >>> integral_part(-1.2)\n",
    "    -1\n",
    "    '''\n",
    "    return int(x // 1)\n",
    "\n",
    "\n",
    "if __name__ == '__main__':\n",
    "    import doctest\n",
    "    doctest.testmod()\n",
    "```\n",
    "\n",
    "When the program is run from the shell, it executes the doctests and reports any failures.\n",
    "A sample interactive session in the shell is as follows.\n",
    "\n",
    "```sh\n",
    "$ python doctest_example.py \n",
    "**********************************************************************\n",
    "File \"doctest_example.py\", line 21, in __main__.integral_part\n",
    "Failed example:\n",
    "    integral_part(-1.2)\n",
    "Expected:\n",
    "    -1\n",
    "Got:\n",
    "    -2\n",
    "**********************************************************************\n",
    "1 items had failures:\n",
    "   1 of   3 in __main__.integral_part\n",
    "***Test Failed*** 1 failures.\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Unit tests\n",
    "Unit tests are simple tests written for specific components of a program.\n",
    "Unit tests provide a means to test parts of your program in isolation.\n",
    "Unit tests encourage writing modular programs.\n",
    "\n",
    "Programs written in functional style are especially amenable to unit tests.\n",
    "Properly designed object oriented programs can also be tested easily with unit tests.\n",
    "\n",
    "The `unittest` module included in the Python standard library provides functionality for\n",
    "writing and running unit tests.\n",
    "\n",
    "\n",
    "## Mocking Objects\n",
    "Sometimes a part of a program depends on a component or service (e.g. database, network)\n",
    "which may not be available during the testing phase.\n",
    "Mocks are objects which simulate certain aspects of the missing component for the purpose of\n",
    "testing."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Python Development Mode\n",
    "Python development mode is a way to run your program from the command line and have the \n",
    "Python interpreter report on issues such as using deprecated features and forgeting to close\n",
    "open files and network connections.\n",
    "\n",
    "The syntax for running your program in development mode is as follows.\n",
    "```sh\n",
    "$ python -X dev myprogram.py\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pytest\n",
    "Pytest is a test framework much like the `unittest` framework.\n",
    "We shall use Pytest to run our unittest because it has slightly better test reporting."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Putting It Together: The Missing Digit Problem\n",
    "We are given a string representing a mathematical equation of the form:\n",
    "$$ A \\otimes B = C $$\n",
    "where $A, B$, and $C$ are integers, and $\\otimes$ is one of $+, -. \\times$, or $/$.\n",
    "A digit in one of the numbers given is marked by an `x`.\n",
    "Our task is to find the missing digit.\n",
    "\n",
    "For example given `'3x + 12 = 46'`, our program should return `4` because `34 + 12 = 46`\n",
    "The `x` character can appear in any of the numbers, and all three numbers will be in the\n",
    "range $[0, 1000000]$ inclusive.\n",
    "\n",
    "## Examples\n",
    "```\n",
    "Input: '4 - 2 = x'\n",
    "Output: 2\n",
    "\n",
    "Input: '1x0 * 12 = 1200'\n",
    "Output: 0\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A Solution Plan\n",
    "1. Parse the input string to get an equation                                \n",
    "2. normalise the equation: collect the known numbers to a single expression\n",
    "3. evaluate the expression with known numbers\n",
    "4. convert the result of step 3 to a string\n",
    "5. compare the result of step 4 character-wise with the number with\n",
    "   the unknown digit to get the unknown digit"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A Functional Programming Style Implementation\n",
    "\n",
    "To solve the problem, we have to come up with a way to represent equations and expressions\n",
    "in our program.\n",
    "Expressions in the missing digit problem comes in two flavours\n",
    "- SingletonExpression : consisting of a single number e.g. `10`, `x2`\n",
    "- CompoundExpression : consisting of two numbers and an operator e.g. `'1 + 2'` `'3x / 6'`\n",
    "\n",
    "We can represent SingletonExpressions as tuples of one element, and CompundExpressions as\n",
    "tuples of three elemets.\n",
    "Furthermore we can represent CompoundExpressions with the following format:\n",
    "```\n",
    "(operator, first_operand, second_operand)\n",
    "```\n",
    "\n",
    "This way `1 + 2` will be `('+', '1', '2')` and `3x / 6` will be `(`/`, '3x', '2')`.\n",
    "\n",
    "Equations can be represented in the form: `(CompoundExpression, SingletonExpression)`.\n",
    "Thus `'1x0 * 12 = 1200'` will be `(('*', '1x0', '12'), ('1200',))`\n",
    "\n",
    "The full functional programming style solution is given in the file `missing_digit_fp.py`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## An Object Oriented Implementation\n",
    "In the oo implementation, we design classes for expressions, equations and operators.\n",
    "As both SingletonExpressions and CompoundExpressions are both expressions, we create an\n",
    "Expression abstract base class which specifies the interface for both SingletonExpressions\n",
    "and CompundExpressions.\n",
    "\n",
    "The oo implementation showcases the following oop concepts:\n",
    "- Inheretance\n",
    "- Dunder methods\n",
    "- Abstract base classes                                                        \n",
    "- Dataclasses\n",
    "- Class variables\n",
    "- Class methods\n",
    "- Static methods\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The programs and their corresponding tests can be found at `ansto-python-workshop/day1/pydev/src/missing_digit`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Linting, Testing and Typechecking the Programs\n",
    "Both programs can be linted using Pylint. The `.pylintrc` file specifies the style guide\n",
    "for the programs.\n",
    "The programs both use gradual typing and have unittests written for them.\n",
    "As such we can run the tests, and check the program for static type errors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# References\n",
    "- Poetry  https://python-poetry.org/docs/\n",
    "- Pylint https://pylint.org/\n",
    "- Type Hints https://www.python.org/dev/peps/pep-0484/#acceptable-type-hints\n",
    "- Typing https://docs.python.org/3/library/typing.html\n",
    "- Benjamin C. Pierce \"Types and Programming Languages\", The MIT Press Cambridge, Massachusetts London, England, 2002.\n",
    "- Mypy http://mypy-lang.org/\n",
    "- Doctests https://docs.python.org/3/library/doctest.html\n",
    "- Python Unittest https://docs.python.org/3/library/unittest.html\n",
    "- Python unittest.mock  https://docs.python.org/3/library/unittest.mock.html\n",
    "- Python Development Mode https://docs.python.org/3/library/devmode.html"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
