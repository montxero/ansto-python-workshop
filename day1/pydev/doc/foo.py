import math

def fractional_part(x):
    '''Return the fractional part of x.
    
    Parameter
    ---------
    x :  number
    
    Returns
    -------
    float
    
    Examples
    --------
    >>> fractional_part(4)
    0
    >>> fractional_part(3.56)
    0.56
    >>> fractional_part(-1.2)
    0.2
    '''
    return x % 1

def integral_part(x):
    '''Return the integral part of x.
    
    Parameter
    ---------
    x :  number
    
    Returns
    -------
    float
    
    Examples
    --------
    >>> integral_part(4)
    4
    >>> integral_part(3.56)
    3
    >>> integral_part(-1.2)
    -1
    '''
    return x // 1

    
    
if '__name__' == '__main__':
    import doctest
    doctest.testmod()
