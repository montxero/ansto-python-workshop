This unit Covers basic Python.
# Setup
The following steps creates an environment suitable for following this
tutorial.

1. In a terminal, navigate to `day1/basic-python` day1.
2. Create a **venv** virtual environment in the current directory with
   `$ python -m venv .venv`
3. Activate the virtual environment.
   - If in powershell: `$ .\.venv\Scripts\activate`
   - In a unix-like os, `$ . .venv/bin/acitvate`
4. Install Jupyterlab with `(.venv)$ pip install jupyterlab`

## Post Setup
You can launch a new Jupyterlab session with `(.venv) $ jupyter-lab`
