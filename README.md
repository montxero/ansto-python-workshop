# Setup
If you do not have the repository already, clone it with
`git clone https://gitlab.com/montxero/ansto-python-workshop`

You can get the most recent updates and versions by updating the repo with
`git pull origin master`
