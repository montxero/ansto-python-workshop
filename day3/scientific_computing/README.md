# Scientific Computing with Python

# Setup
This session requires Jupyter notebook extensions.
1. Navigate to the project directory in a shell.
2. In the shell, run the following in sequence.
    ```sh
    $ poetry config virtualenvs.in-project true
    $ poetry update
    $ poetry shell
    ```
3. Start Jupyter lab with `jupyter lab`
4. Open the notebook in the browser.
