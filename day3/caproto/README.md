# Hardware Control With Caproto

## Setup
In a terminal, navigate to the project directory
```sh
$ cd path-to-project-directory
```
Create a virtual environment
```sh
$ python -m venv .venv
```
Activate the environment:
- Windows users `$ .\.venv\Scripts\activate`
- *nix users `$ . .venv/bin/activate`

Install caproto, numpy, matplotlib and jupyter lab.
```sh
(.venv)$ pip install -U caproto numpy matplotlib jupyterlab
```
