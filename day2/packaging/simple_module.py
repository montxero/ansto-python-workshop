'''simple_module.py'''

print(f'Arbitrary Python code can be executed when a module is imported!!!')

def add(x, y):
    '''return x+y '''
    return x + y


def invert(x):
    '''Return 1/x'''
    return 1/x
