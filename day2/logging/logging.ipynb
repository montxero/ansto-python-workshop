{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At the end of this session, you will understand\n",
    "\n",
    "- logging\n",
    "- configuring a logger\n",
    "- the different logging levels\n",
    "- logging to a file and console\n",
    "- using a logger across multiple modules."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Setup\n",
    "See the README file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Logging\n",
    "\n",
    "For our purposes, logging means collecting information about a program as it\n",
    "runs, and storing the information in a way that can be easily analysed later.\n",
    "\n",
    "Where debuggers like `pdb` allow us interact with our running program, logging,\n",
    "stores interesting information which we want to analyse after the program is run.\n",
    "\n",
    "\n",
    "The builtin `logging` module provides functionality for logging in Python programs.\n",
    "With the logging module, you can create and configure a logger, then use the same logger\n",
    "across several modules and threads!\n",
    "\n",
    "# Logging levels\n",
    "The logging module comes with 6 built in levels each with a unique numeric value.\n",
    "\n",
    "|Level | Numeric value|\n",
    "|:--:|:--:|\n",
    "|CRITICAL| 50|\n",
    "|ERROR | 40|\n",
    "|WARNING | 30|\n",
    "|INFO | 20|\n",
    "|DEBUG | 10|\n",
    "|NOTSET | 10|\n",
    "\n",
    "When configuring the logger, we can set the default level of the logger with the `level` key.\n",
    "\n",
    "\n",
    "The workflow for using the logging module is as follows\n",
    "1. import logging\n",
    "2. configure the logger\n",
    "3. create a logger\n",
    "4. use the logger.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Hello Logging\n",
    "\n",
    "We create a log entry by calling the method associated with the logging level for our\n",
    "message."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%file hello_logging.py\n",
    "'''hello_logging.py'''\n",
    "\n",
    "\n",
    "import logging                # import logging\n",
    "\n",
    "\n",
    "logging.basicConfig()         # configure the logger\n",
    "logger = logging.getLogger()  # create a logger\n",
    "\n",
    "# use the logger\n",
    "logger.debug('Hello debug')\n",
    "logger.info('Hello info')\n",
    "logger.warning('Hello warning')\n",
    "logger.error('Hello error')\n",
    "logger.critical(\"Hello critical\")\n",
    "\n",
    "print(f'Logging level = {logger.level}')  # show the logger's level"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run `hello_logging.py` and observe the output."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice a few things\n",
    "1. Only the WARNING, ERROR and CRITICAL level events were logged.\n",
    "2. The logs were printed out in the console\n",
    "3. The format of each log entry `Level:logger_name:message`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The logging level\n",
    "To see the level of a logger, lookup the level attribute.\n",
    "The logger will only report events which are equal to or greater than the logger level.\n",
    "\n",
    "From the logging levels table, we see the level corresponds to WARNING.\n",
    "To change the level of the logger, we set the value with the `level` keyword\n",
    "when we configure the logger"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%file hello_logging_level.py\n",
    "'''hello_logging_level.py'''\n",
    "\n",
    "\n",
    "import logging                              # import logging\n",
    "\n",
    "\n",
    "logging.basicConfig(level=logging.DEBUG)    # configure the logger\n",
    "logger = logging.getLogger()                # create a logger\n",
    "\n",
    "# use the logger\n",
    "logger.debug('Hello debug')\n",
    "logger.info('Hello info')\n",
    "logger.warning('Hello warning')\n",
    "logger.error('Hello error')\n",
    "logger.critical(\"Hello critical\")\n",
    "\n",
    "print(f'Logging level = {logger.level}')  # show the logger's level"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run `hello_logging_level.py` and observe the output."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The Logging Format\n",
    "The logging format can be defined using LogRecord attributes https://docs.python.org/3/library/logging.html?highlight=logging#logrecord-attributes.\n",
    "\n",
    "Let us set the log entry format to be `*** Levelname, Filename::Lineno, message` "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%file hello_logging_format.py\n",
    "'''hello_logging_format.py'''\n",
    "\n",
    "\n",
    "import logging                             \n",
    "\n",
    "\n",
    "LOG_FORMAT = '*** %(levelname)s %(filename)s::%(lineno)d %(message)s'\n",
    "logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)\n",
    "logger = logging.getLogger()\n",
    "\n",
    "# use the logger\n",
    "logger.debug('Hello debug')\n",
    "logger.info('Hello info')\n",
    "logger.warning('Hello warning')\n",
    "logger.error('Hello error')\n",
    "logger.critical(\"Hello critical\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Logging to a File\n",
    "To write the logs to a file, simply pass the file name to the configuration method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%file hello_logging_file.py\n",
    "'''hello_logging_file.py'''\n",
    "\n",
    "\n",
    "import logging                             \n",
    "\n",
    "# set log file name to name of current file with .log suffix\n",
    "# instead of .py\n",
    "# the __file__ variable returns the name of the current file.\n",
    "\n",
    "LOG_FILE = __file__[:-2] + 'log'  \n",
    "LOG_FORMAT = '*** %(levelname)s %(filename)s::%(lineno)d %(message)s'\n",
    "logging.basicConfig(level=logging.DEBUG,\n",
    "                    format=LOG_FORMAT,\n",
    "                    filename=LOG_FILE)\n",
    "logger = logging.getLogger()\n",
    "\n",
    "logger.debug('Hello debug')\n",
    "logger.info('Hello info')\n",
    "logger.warning('Hello warning')\n",
    "logger.error('Hello error')\n",
    "logger.critical(\"Hello critical\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Putting it together: A multifile example\n",
    "From the following statements:\n",
    "1. 0 is an even number\n",
    "2. If a non-negative integer $n$ is even, then $n - 1$ is odd.\n",
    "3. If a non-negative integer $n$ is odd, then $n - 1$ is even.\n",
    "\n",
    "We can define a pair of mutualy recursive funtions to check if a number is even or odd."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def is_even(n):\n",
    "    '''Retrun True iff n is even'''\n",
    "    if n == 0:\n",
    "        return True\n",
    "    return is_odd(n-1)\n",
    "\n",
    "def is_odd(n):\n",
    "    '''Retrun True iff n is odd'''\n",
    "    if n == 0:\n",
    "        return False\n",
    "    return is_even(n-1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets try it out."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f'Even: {[i for i in range(10) if is_even(i)]}')\n",
    "print(f'Odd:  {[i for i in range(10) if is_odd(i)]}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The structure\n",
    "\n",
    "Now, let us create 3 modules, `even.py`, `odd.py` and `even_odd.py`.\n",
    "\n",
    "The module `even_odd.py` contains a function `main` which accepts command line arguments \n",
    "`n` and `-f` and calls `is_even(n)` if `-f` is  `'even'` otherwise it calls `is_odd(n)` \n",
    "if `-f` is `'odd'`.\n",
    "\n",
    "For example to check if 5 is even, from the command line, we will run our program as follows:\n",
    "\n",
    "```sh\n",
    "$ python even_odd.py -i 5 -f even\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In `even_odd.py`, we configure a logger which we use in both `even.py` and `odd.py`.\n",
    "\n",
    "The source for the programs are as follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%file even.py\n",
    "'''even.py'''\n",
    "import logging\n",
    "\n",
    "\n",
    "def is_even(n):\n",
    "    '''Return True iff n is even'''\n",
    "    from odd import is_odd  # done here to avoid circular dependencies at the module level\n",
    "    try:\n",
    "        if n == 0:\n",
    "            return True\n",
    "        else:\n",
    "            logging.info(f'is {n} even?')\n",
    "            return is_odd(n-1)\n",
    "    except RecursionError as e:\n",
    "        logging.critical('Maximum recursion Error!')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%file odd.py\n",
    "'''odd.py'''\n",
    "import logging\n",
    "\n",
    "\n",
    "def is_odd(n):\n",
    "    '''Return True iff n is odd'''\n",
    "    from even import is_even   # import in function to avoid circular dependencies\n",
    "    try:\n",
    "        if n == 0:\n",
    "            return False\n",
    "        else:\n",
    "            logging.info(f'is {n} odd?')\n",
    "            return is_even(n-1)\n",
    "    except RecursionError as e:\n",
    "        logging.critical('Maximum recursion Error!')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%file even_odd.py\n",
    "'''even_odd.py'''\n",
    "import logging\n",
    "from even import is_even\n",
    "from odd import is_odd\n",
    "\n",
    "\n",
    "LOG_FILE = __file__[:-2] + 'log'  \n",
    "LOG_FORMAT = '*** %(levelname)s %(funcName)s %(message)s'\n",
    "\n",
    "def main(n, function_name='even'):\n",
    "    '''Return is_even(n) if function_name is even.\n",
    "    Return is_odd(n) if function_name is odd.\n",
    "    '''\n",
    "    logging.basicConfig(level=logging.DEBUG,\n",
    "                        format=LOG_FORMAT,\n",
    "                        filename=LOG_FILE)\n",
    "    if function_name == 'even':\n",
    "        return is_even(n)\n",
    "    elif function_name == 'odd':\n",
    "        return is_odd(n)\n",
    "\n",
    "\n",
    "if __name__ == '__main__':\n",
    "    import argparse\n",
    "    \n",
    "    DESC = 'Program to check if a number is even or odd'\n",
    "    parser = argparse.ArgumentParser(prog='even_odd',\n",
    "                                     description=DESC)\n",
    "    parser.add_argument('-f',\n",
    "                        action='store',\n",
    "                        type=str,\n",
    "                        help='function name to pass to main',\n",
    "                        default='even')\n",
    "    parser.add_argument('-n',\n",
    "                        action='store', \n",
    "                        type=int, \n",
    "                        required=True,\n",
    "                        help='number to check')\n",
    "    args = parser.parse_args()\n",
    "    main(args.n, args.f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the program with different arguments, and inspect the resulting log file.\n",
    "Try tweaking the logger level and reporting formats."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# References\n",
    "\n",
    "- Python Logging https://docs.python.org/3/library/logging.html?highlight=logging#module-logging\n",
    "- Python Logging Tutorial https://docs.python.org/3/howto/logging.html\n",
    "- Python Logging Cookbook https://docs.python.org/3/howto/logging-cookbook.html#logging-cookbook"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
